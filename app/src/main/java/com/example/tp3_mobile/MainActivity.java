package com.example.tp3_mobile;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import com.example.tp3_mobile.data.Team;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.example.tp3_mobile.data.SportDbHelper;

public class MainActivity extends AppCompatActivity
{

    public SportDbHelper sportHelp;
    public SQLiteDatabase db;
    private SimpleCursorAdapter sca;
    Intent myIntent;
    Cursor result;
    ListView team;
    static MainActivity MAIN;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        MAIN = this;

        sportHelp = new SportDbHelper(this);

        UpdateCursor();

        team.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                result.moveToPosition(position);

                Team equipe = sportHelp.cursorToTeam(result);

                myIntent = new Intent(MainActivity.this, TeamActivity.class).putExtra("Team", equipe);

                startActivityForResult(myIntent, 2);
            }
        });


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myIntent = new Intent(MainActivity.this, NewTeamActivity.class);

                startActivityForResult(myIntent, 1);
                //Snackbar.make(view, "Lights, Camera, Action!!", Snackbar.LENGTH_LONG)
                //       .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void UpdateCursor(){
        result=sportHelp.fetchAllTeams();

        String[] columns = new String[] {
                SportDbHelper.COLUMN_TEAM_NAME,
                SportDbHelper.COLUMN_LEAGUE_NAME
        };

        int[] to = new int[]{
                R.id.ligne_a,
                R.id.ligne_b
        };

        sca = new SimpleCursorAdapter(this, R.layout.twolignes, result, columns, to, 0);

        team=(ListView)findViewById(R.id.listTeam);
        team.setAdapter(sca);

    }

    @Override
    public void onResume() {
        super.onResume();

        UpdateCursor();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==1)
        {
            Team newTeam= data.getParcelableExtra(Team.TAG);

            sportHelp.addTeam(newTeam);
            UpdateCursor();
        }
        else if(requestCode==2)
        {
            Team updateTeam= data.getParcelableExtra(Team.TAG);

            sportHelp.updateTeam(updateTeam);

            UpdateCursor();
        }
    }

}
