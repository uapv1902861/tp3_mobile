package com.example.tp3_mobile.webservice;

import android.util.JsonReader;
import android.util.JsonToken;
import android.util.Log;

import com.example.tp3_mobile.data.Team;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class JSONResponseHandlerScore {
    private static final String TAG = JSONResponseHandlerScore.class.getSimpleName();

    private Team team;

    public JSONResponseHandlerScore(Team team) {
        this.team = team;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readTeams(reader);
        } finally {
            reader.close();
        }
    }

    public void readTeams(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("table")) {
                readArrayScore(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }


    private void readArrayScore(JsonReader reader) throws IOException {
        reader.beginArray();
        int nb=0;
        String nom = "";
        while (reader.hasNext() ) {
            reader.beginObject();
            String n = reader.nextName();
            nom = reader.nextString();
            if (nom.equals(team.getName())) {
                while (reader.hasNext()) {
                    String name = reader.nextName();
                    if (name.equals("total")) {
                        team.setTotalPoints(reader.nextInt());
                    } else {
                        reader.skipValue();
                    }
                }
            }
            else{
                while(reader.hasNext()){
                    reader.skipValue();
                }
            }
            reader.endObject();
            if (team.getTotalPoints() == 0) {
                nb++;
            }
        }
        reader.endArray();

        team.setRanking(nb);
    }
}
