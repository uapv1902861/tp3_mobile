package com.example.tp3_mobile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.tp3_mobile.data.Match;
import com.example.tp3_mobile.data.Team;
import com.example.tp3_mobile.webservice.JSONResponseHandlerMatch;
import com.example.tp3_mobile.webservice.JSONResponseHandlerScore;
import com.example.tp3_mobile.webservice.JSONResponseHandlerTeam;
import com.example.tp3_mobile.webservice.WebServiceUrl;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class TeamActivity extends AppCompatActivity {

    private static final String TAG = TeamActivity.class.getSimpleName();
    private TextView textTeamName, textLeague, textManager, textStadium, textStadiumLocation, textTotalScore, textRanking, textLastMatch, textLastUpdate;
    ProgressDialog progressDialog;

    private int totalPoints;
    private int ranking;
    private Match lastEvent;
    private String lastUpdate;

    private ImageView imageBadge;
    private Team team;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);

        team = (Team) getIntent().getParcelableExtra(Team.TAG);

        textTeamName = (TextView) findViewById(R.id.nameTeam);
        textLeague = (TextView) findViewById(R.id.ligne_b);
        textStadium = (TextView) findViewById(R.id.editStadium);
        textStadiumLocation = (TextView) findViewById(R.id.editStadiumLocation);
        textTotalScore = (TextView) findViewById(R.id.editTotalScore);
        textRanking = (TextView) findViewById(R.id.editRanking);
        textLastMatch = (TextView) findViewById(R.id.editLastMatch);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);

        imageBadge = (ImageView) findViewById(R.id.imageView);


        updateView();

        final Button but = (Button) findViewById(R.id.button);

        final TeamActivity[] asyncInput = new TeamActivity[]{this};
        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
            // TODO
                MyAsyncTasks asyncTask = new MyAsyncTasks();
                asyncTask.execute();
            }
        });

    }

    @Override
    public void onBackPressed() {
        //TODO : prepare result for the main activity
        Intent intent = new Intent();

        intent.putExtra(Team.TAG, team);
        setResult(2, intent);

        super.onBackPressed();
    }

    private void updateView()
    {

        textTeamName.setText(team.getName());
        textLeague.setText(team.getLeague());
        textStadium.setText(team.getStadium());
        textStadiumLocation.setText(team.getStadiumLocation());
        textTotalScore.setText(Integer.toString(team.getTotalPoints()));
        textRanking.setText(Integer.toString(team.getRanking()));
        textLastMatch.setText(team.getLastEvent().toString());
        textLastUpdate.setText(team.getLastUpdate());


	//TODO : update imageBadge
    }

    public class MyAsyncTasks extends AsyncTask<String, String, Team> {


        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            progressDialog = new ProgressDialog(TeamActivity.this);
            progressDialog.setMessage("Mise à jours en cours...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Team doInBackground(String... params)
        {
            Team updateTeam = new Team(" ", " ");
            Match updateMatch = new Match();
            try
            {
                URL url;
                JSONResponseHandlerTeam JSONTeam = new JSONResponseHandlerTeam(updateTeam);
                JSONResponseHandlerMatch JSONMatch = new JSONResponseHandlerMatch(updateMatch);
                HttpURLConnection urlConnection = null;

                try
                {
                    url = WebServiceUrl.buildSearchTeam(textTeamName.getText().toString());

                    urlConnection = (HttpURLConnection) url.openConnection();

                    InputStream in = urlConnection.getInputStream();

                    JSONTeam.readJsonStream(in);

                }
                catch (MalformedURLException e)
                {
                    e.printStackTrace();
                }

                try
                {
                    url = WebServiceUrl.buildSearchLastEvents(updateTeam.getIdTeam());

                    urlConnection = (HttpURLConnection) url.openConnection();
                    InputStream in = urlConnection.getInputStream();

                    JSONMatch.readJsonStream(in);

                }
                catch (MalformedURLException e)
                {
                    e.printStackTrace();
                }

                try
                {
                    JSONResponseHandlerScore JSONScore = new JSONResponseHandlerScore(updateTeam);
                    url = WebServiceUrl.buildGetRanking(updateTeam.getIdLeague());

                    urlConnection = (HttpURLConnection) url.openConnection();
                    InputStream in = urlConnection.getInputStream();

                    JSONScore.readJsonStream(in);

                }
                catch (MalformedURLException e)
                {
                    e.printStackTrace();
                }

            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

            updateTeam.setLastEvent(updateMatch);

            return updateTeam;
        }

        @Override
        protected void onPostExecute(Team t)
        {
            progressDialog.dismiss();
            t.setId(team.getId());
            team = t;
            updateView();
        }

    }

}

